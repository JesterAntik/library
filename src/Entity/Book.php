<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity(repositoryClass="App\Repository\BookRepository")
 * @UniqueEntity(
 *     fields={"title", "year"},
 *     errorPath="title",
 *     message="Такая книга уже существует в каталоге (Название и год издания)"
 * )
 * @UniqueEntity(
 *     fields={"isbn"},
 *     errorPath="title",
 *     message="Такая книга уже существует в каталоге (ISBN)"
 * )
 */
class Book
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Не бывает книги без названия")
     */
    private $title;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\Type(
     *     type="integer",
     *     message="Укажите год числом (если книга издана до н.э., то укажите год со знаком -)"
     * )
     * @Assert\LessThan(
     *     value=2020,
     *     message="Это каталог для уже напечатанных книг"
     *     )
     *
     */
    private $year;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     * @Assert\Positive (message="ISBN может быть только положительным числом")
     */
    private $isbn;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Positive (message="Укажите количество страниц конкретным числом")
     */
    private $size;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cover;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Author", inversedBy="books")
     */
    private $authors;

    public function __construct()
    {
        $this->authors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getIsbn(): ?int
    {
        return $this->isbn;
    }

    public function setIsbn(?int $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(int $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * @return Collection|Author[]
     */
    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function addAuthor(Author $author): self
    {
        if (!$this->authors->contains($author)) {
            $this->authors[] = $author;
        }

        return $this;
    }

    public function removeAuthor(Author $author): self
    {
        if ($this->authors->contains($author)) {
            $this->authors->removeElement($author);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->title;
    }
}
